
//****************************** CLASS MESSAGIN ********************************
class FireBaseMessage {
  String title;
  String body;
  //String location;

  //******************************* CONSTRUCT **********************************
  FireBaseMessage();

  //************************** CONVERT TO JSON *********************************
  FireBaseMessage.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        body = json['body'];

  //************************** CONVERT TO JSON *********************************
  Map<String, dynamic> toJson() => {
    'title': title,
    'body': body,
  };
}
//******************************************************************************
