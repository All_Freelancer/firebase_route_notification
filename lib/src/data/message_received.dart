import 'dart:async';
import 'package:firebase_route_notification/src/ui/pages/notification.dart';
import 'package:flutter/material.dart';

//Model class to represent the message return by FCM
//******************************* CLASS MESSAGE RECEIVED ***********************
class MessageReceived {
  //Variable
  final String itemId;
  String _status;

  StreamController<MessageReceived> _controller = StreamController<MessageReceived>.broadcast();
  static final Map<String, Route<void>> routes = <String, Route<void>>{};

  //************************** CONSTRUCT ***********************************
  MessageReceived({this.itemId});

  //*************************** METHOD GET **********************************
  //:::::::::: STATUS :::::::::
  String get status => _status;

  //:::::::::: ON CHANGED ::::::
  Stream<MessageReceived> get onChanged => _controller.stream;

  //:::::::::: ROUTE ::::::::::::
  Route<void> get route {
    final String routeName = '/detail/$itemId';

    return routes.putIfAbsent(routeName, () => MaterialPageRoute<void>(
        settings: RouteSettings(name: routeName),
        builder: (BuildContext context) => MessageNotification(itemId),
      ),
    );
  }

  //**************************** SET STATUS ********************************
  set status(String value) {
    _status = value;
    _controller.add(this);
  }

}
//******************************************************************************
