import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_route_notification/src/data/message_received.dart';
import 'package:firebase_route_notification/src/data/shared_pref.dart';
import 'package:firebase_route_notification/src/model/firebase_message.dart';
import 'package:firebase_route_notification/src/ui/pages/notification.dart';
import 'package:flutter/material.dart';
import 'package:firebase_route_notification/src/ui/pages/home.dart';

import '../../const.dart';

//***************************** CLASS NAVIGATION *******************************
class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

//******************************* STATE NAVIGATION *****************************
class _NavigationState extends State<Navigation> {
   //Variable
  bool _newNotification = false;

  int _selectedTab = 0;
  final _pageOptions = [Home(), MessageNotification(null),];
  final _selectText = ["Home", "Notification Message"];

  SharedPref sharedPref = SharedPref();
  FireBaseMessage notificationSave = FireBaseMessage();

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();


  // ignore: missing_return
  Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  //****************************** INIT STATE **********************************
  @override
  void initState() {
    super.initState();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");

        final notification = message['notification'];
        notificationSave.title = notification['title'];
        notificationSave.body = notification['body'];
        sharedPref.save("message", notificationSave);

        setState(() {
          _newNotification = true;
        });

      },

      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");

        //_navigateToItemDetail(message);
        _onItemTapped(1);
      },

      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");

        _onItemTapped(1);
        /*
        setState(() {
          _navigateToItemDetail(message);
        });

         */
      },
    );

    //Needed by iOS only - PERMISSION
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));

    //this create token for
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    //Getting the token from FCM
    _firebaseMessaging.getToken().then((String token) {
      print(token);
    });

  }

  //PRIVATE METHOD TO HANDLE NAVIGATION TO SPECIFIC PAGE
  void _navigateToItemDetail(Map<String, dynamic> message) {
    final MessageReceived item = _itemForMessage(message);

    // Clear away dialogs
    //Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);

    if (!item.route.isCurrent) {
      Navigator.push(context, item.route);
    }
  }

  //************************
  MessageReceived _itemForMessage(Map<String, dynamic> message) {
    //If the message['data'] is non-null, we will return its value, else return map message object
    final dynamic data = message['data'] ?? message;
    final String itemId = data['id'];

    final MessageReceived item = ConstGlobal.items.putIfAbsent(
        itemId, () => MessageReceived(itemId: itemId))
      ..status = data['status'];

    return item;
  }

  //*************************** WIDGET ROOT HOME *******************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //::::::::::::::::::::::::: TITLE BAR ::::::::::::::::::::::::::::
        appBar: AppBar(
          title: Text(_selectText[_selectedTab]),
        ),

        //::::::::::::::::::::::: PAGE VIEW CURRENT ::::::::::::::::::::::
        body: _pageOptions[_selectedTab],

        //:::::::::::::::::::::::: NAVIGATION :::::::::::::::::::::::::::::
        bottomNavigationBar: BottomNavigationBar(

          //::::::::::::::::: SELECT PAGE :::::::::::::::
          currentIndex: _selectedTab,         //SELECT CURRENT
          selectedItemColor: Colors.green,    //COLOR SELECTED
          onTap: _onItemTapped,               //NEW SELECT

          //:::::::::::::: ITEMS NAVIGATION BAR ::::::::::
          items: [
            //----------- HOME -------------
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
            ),

            //----------- NOTIFICATION -----
            BottomNavigationBarItem(
              //... Test if exit new notification
              icon: _newNotification ? Stack( children: <Widget>[
                Icon(Icons.notifications),
                  Positioned(
                    right: 0,
                    child: Container(
                      padding: EdgeInsets.all(1),
                      decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(15),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 13,
                        minHeight: 13,
                      ),
                      child: Text(
                        '',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                ],
              )
                  //............ No notification
                  : Icon(Icons.notifications),

              title: Text('Notifications'),
            ),
          ],
          
        ),
    );
  }

  //PRIVATE METHOD TO HANDLE TAPPED EVENT ON THE BOTTOM BAR ITEM
  void _onItemTapped(int index) {
    setState(() {
      _selectedTab = index;
      if (index == 1){
        _newNotification = false ;
      }
    });
  }

}
//******************************************************************************
