import 'dart:async';
import 'package:firebase_route_notification/src/data/message_received.dart';
import 'package:firebase_route_notification/src/data/shared_pref.dart';
import 'package:firebase_route_notification/src/model/firebase_message.dart';
import 'package:flutter/material.dart';

//***************************** CLASS NAVIGATION *******************************
class MessageNotification extends StatefulWidget {
  final String itemId;

  //***************************** CONSTRUCT *******************************
  MessageNotification(this.itemId);

  //***************************** CREATE STATE ****************************
  @override
  _MessageNotificationState createState() => _MessageNotificationState();
}

//****************************** STATE NOTIFICATION ****************************
class _MessageNotificationState extends State<MessageNotification> {

  MessageReceived _item;
  StreamSubscription<MessageReceived> _subscription;

  SharedPref sharedPref = SharedPref();
  FireBaseMessage notificationLoad = FireBaseMessage();

  //**************************** METHOD LOAD SHARED PREF ***********************
  loadSharedPrefs() async {
    try {
      FireBaseMessage user =
      FireBaseMessage.fromJson(await sharedPref.read("message"));

      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Loaded!"),
          duration: const Duration(milliseconds: 500)));

      //:::::::::::::::: SET STATE :::::::
      setState(() {
        notificationLoad = user;
      });
    } catch (Excepetion) {
      //:::::::::::::::::: SNACK BAR ::::::::::::::::::
      Scaffold.of(context).showSnackBar(SnackBar(
          content: new Text("Nothing found!"),
          duration: const Duration(milliseconds: 500)));
    }
  }

  //****************************** INIT STATE **********************************
  @override
  void initState() {
    super.initState();

    loadSharedPrefs();

    /*
    _item = ConstGlobal.items[widget.itemId];

    _subscription = _item.onChanged.listen((MessageReceived item) {

      if (!mounted) {
        _subscription.cancel();
      } else {
        setState(() {
          _item = item;
        });
      }
    });
    */
  }

  //************************* WIDGET ROOT NOTIFICATION *************************
  /*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Item ${_item.itemId}"),

      ),
      body: Material(
        child: Center(
            child: Text("Item status: ${_item.status}")),
      ),
    );
  }*/

  Widget build(BuildContext context) {
    //:::::::::::::::::::::::::::: CARD BUILD ITEM ::::::::::::::::::::::::::::

    if(notificationLoad.title != null)
      return ListView(
        padding: const EdgeInsets.all(10),

        children: <Widget>[
          Card(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${notificationLoad.title}',
                    style: TextStyle(fontSize: 24),
                  ),
                  Text(
                    '${notificationLoad.body}',
                    style: TextStyle(fontSize: 20),
                  ),
                  SizedBox(height: 12),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      FlatButton(
                        //onPressed: () =>  sharedPref.remove("message"),

                        onPressed: () {
                          sharedPref.remove("message");
                          setState(() {
                            notificationLoad = FireBaseMessage();
                          });
                        },
                        color: Colors.green,
                        child: Text('Apagar'),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      );
    return Container();
  }
}
//******************************************************************************
