import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

import 'package:firebase_route_notification/src/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';

//***************************** CLASS MAIN *************************************
void main() async {
  runApp(MyApp());
}
//****************************** CLASS ROOT ************************************
class MyApp extends StatelessWidget {
  final String appTitle = 'Firebase messaging';

  FirebaseAnalytics analytics = FirebaseAnalytics();

  
  //***************************** WIDGETS ROOT *********************************
  @override
  Widget build(BuildContext context) => MaterialApp(
    title: appTitle,

    //:::::::::::::::::: DEFAULT ALL APP :::::::::::


    home: Navigation(),   //CALL HOME

    navigatorObservers: [
      FirebaseAnalyticsObserver(analytics: analytics),
    ],

  );
}
//******************************************************************************
